/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Matt
 */
public class DiscountFactory {
    private static DiscountFactory discFactory;
    
    private DiscountFactory(){
            
    }
    
     public static DiscountFactory getInstance(){
        if(discFactory == null){
            discFactory = new DiscountFactory();
        }
        return discFactory;
    }
     
     public Discount getDiscount(DiscountType type, double discountAmount){
        Discount discount = null;
        switch (type){
            case AMOUNT:
                discount = new DiscountByAmount(discountAmount);
                break;
            case PERCENTAGE:
                discount = new DiscountByPercentage(discountAmount);
                break;
        }
        return discount;
    }

}
