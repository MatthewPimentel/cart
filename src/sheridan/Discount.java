/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Matt
 */
abstract class Discount {

    
    double discountAmount;

    public Discount(double discountAmount) {
        this.discountAmount = discountAmount;
    }
    
    
    
    abstract double calculateDiscount(double amount);

    public double getDiscountAmount() {
        return discountAmount;
    }
        
    
}


