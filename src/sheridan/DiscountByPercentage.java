/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Matt
 */
public class DiscountByPercentage extends Discount{

    public DiscountByPercentage(double discountAmount) {
        super(discountAmount);
    }
    

    @Override
    double calculateDiscount(double amount) {
        return ( amount - (amount * discountAmount));
    }
    
    
}
